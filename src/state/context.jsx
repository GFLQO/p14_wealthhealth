import React, { useEffect, useState } from 'react';
import { createContext } from 'react';

export const Context = createContext();

/**
 * Provides all data to the entire React App
 * @function ProviderContext
 * @return {data} data
 */
export const Provider = ({ children }) => {
  const [headers, setHeaders] = useState([]);
  const [data, setData] = useState([]);
  useEffect(() => {
    console.log('Data : ', data);
  }, [data]);

  return (
    <Context.Provider
      value={{
        headers,
        setHeaders,
        data,
        setData,
      }}
    >
      {children}
    </Context.Provider>
  );
};
