import './App.css';
import RouterComponent from './utils/router';
import { Provider } from './state/context';


function App() {
  return (
    <div className="App">
      <Provider>
        <RouterComponent />
      </Provider>
    </div>
  );
}

export default App;
