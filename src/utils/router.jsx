import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomeForm from '../pages/HomeForm';
import Tab from '../pages/Tab';

function RouterComponent() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<HomeForm />} />
        <Route exact path="/data" element={<Tab />} />
      </Routes>
    </Router>
  );
}

export default RouterComponent;
