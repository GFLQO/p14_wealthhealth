import React, { useContext, useEffect } from 'react';
import ReactDataTable from 'ocr-react-datatable';
import { Context } from '../state/context';
import styled from 'styled-components';
//import ReactDataTable from '../component/ReactDataTable';
import { Link } from 'react-router-dom';

const Container = styled.div`
  margin: auto;
  display: flex;
  flex-direction: column;
  margin-top: 15px;
  margin-bottom: 25px;
  min-height: 90vh;
`;

const Back = styled.a`
  color: darkgray;
`;

export default function Tab() {
  const { data } = useContext(Context);
  console.log(data);
  const default_sens = 1;
  const default_Active = false;
  const payload = {
    headers: [
      {
        name: 'First Name',
        sens: default_sens,
        type: 'string',
        isActive: default_Active,
      },
      {
        name: 'Last Name',
        sens: default_sens,
        type: 'number',
        isActive: default_Active,
      },
      {
        name: 'StartDate',
        sens: default_sens,
        type: 'date',
        isActive: default_Active,
      },
      {
        name: 'Department',
        sens: default_sens,
        type: 'string',
        isActive: default_Active,
      },
      {
        name: 'Date of Birth',
        sens: default_sens,
        type: 'date',
        isActive: default_Active,
      },
      {
        name: 'Street',
        sens: default_sens,
        type: 'string',
        isActive: default_Active,
      },
      {
        name: 'City',
        sens: default_sens,
        type: 'string',
        isActive: default_Active,
      },
      {
        name: 'State',
        sens: default_sens,
        type: 'string',
        isActive: default_Active,
      },
      {
        name: 'Zip Code',
        sens: default_sens,
        type: 'number',
        isActive: default_Active,
      },
    ],
  };

  return (
    <Container>
      <h1>Current employees</h1>
      <ReactDataTable headers={payload.headers} data={data} debug={true} />
      <Link to={'../'} style={{ textAlign: 'left', marginLeft: '2rem' }}>
        {' '}
        BACK
      </Link>
    </Container>
  );
}

export function Mock() {
  return;
}
