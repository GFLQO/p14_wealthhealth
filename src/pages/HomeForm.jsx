import React, { useContext, useState } from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import ReactDatePicker from 'react-datepicker';
import { states } from '../data/countries';
import Modal from 'react-modal';
import 'react-datepicker/dist/react-datepicker.css';
import { Link } from 'react-router-dom';
import { Context } from '../state/context';
const Container = styled.div`
  max-width: 300px;
  margin: auto;
  display: flex;
  flex-direction: column;
`;
const Column = styled.div`
  display: flex;
  flex-direction: column;
`;

const Label = styled.label`
  max-width: 150px;
  text-align: start;
  display: block;
  margin-top: 1rem;
  margin-bottom: 10px;
`;

const Form = styled.form`
  display: flex;
`;

const Input = styled.input`
  max-width: 180px;
  height: 20px;
`;

const Legend = styled.legend`
  margin-left: -40px;
  display: block;
  padding-inline-start: 2px;
  padding-inline-end: 2px;
  border-width: initial;
  border-style: none;
  border-color: initial;
  border-image: initial;
`;

const Option = styled.option`
  display: block;
  margin-right: 20px;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const AdressContainer = styled.fieldset`
  margin-top: 10px;
  margin-inline-start: 2px;
  margin-inline-end: 2px;
  padding-block-start: 0.35em;
  padding-inline-start: 0.75em;
  padding-inline-end: 0.75em;
  padding-block-end: 0.625em;
  border: 1px solid black;
`;

const Select = styled.select`
  border-radius: 3px;
  height: 35px;
  background-color: #f6f6f6;
  outline: none;
  width: 250px;
  color: #454545;
  font-size: 15px;
  border-color: rgba(118, 118, 118, 0.4);
`;

const InputSubmit = styled.input`
  max-width: 75px;
  margin: 15px auto;
`;

const DatePickerWrapperStyles = createGlobalStyle`
    .react-datepicker-wrapper {
        margin-left: 0 ;
        text-align: start;
    }
`;

export default function HomeForm() {
  const departments = [
    'Sales',
    'Marketing',
    'Legal',
    'Engineer',
    'Human Resources',
  ];
  const [modalIsOpen, setIsOpen] = useState(false);
  const [startDate, setStartDate] = useState(new Date());
  const [birthDate, setBirthDate] = useState(new Date());
  const [firstName, setfirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [street, setStreet] = useState('');
  const [city, setCity] = useState('');
  const [state, setState] = useState('');
  const [department, setDepartment] = useState('');
  const [zipCode, setZipCode] = useState(0);

  const { setData } = useContext(Context);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const formatDate = (d) => {
    console.log(d, d instanceof Date);
    return [d.getDate(), d.getMonth(), d.getFullYear()].join('/');
  };

  const modalStyles = {
    content: {
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      display: 'inline-table',
      padding: '15px',
    },
  };

  const addEmployee = () => {
    var isValidForm = true;
    var payload = [
      firstName,
      lastName,
      formatDate(startDate),
      department,
      formatDate(birthDate),
      street,
      city,
      state,
      zipCode,
    ];

    if (firstName === '' || firstName === null) {
      isValidForm = false;
      console.log('firstName is false');
    }
    if (lastName === '' || lastName === null) {
      isValidForm = false;
      console.log('lastName is false');
    }
    if (birthDate === '' || birthDate === null) {
      isValidForm = false;
      console.log('birthDate is false');
    }
    if (department === '' || department === null) {
      isValidForm = false;
      console.log('department is false');
    }
    if (street === '' || street === null) {
      isValidForm = false;
      console.log('street is false');
    }
    if (city === '' || city === null) {
      isValidForm = false;
      console.log('city is false');
    }
    if (zipCode === 0 || zipCode === null) {
      isValidForm = false;
      console.log('zipCode is false');
    }
    if (state === '' || state === null) {
      isValidForm = false;
      console.log('state is false', state);
    }

    if (isValidForm) {
      console.log('C VALIDE', payload);
      setData((current) => {
        return [...current, payload];
      });
    }
  };

  const settttIt = (value) => {
    console.log(value);
    setDepartment(value);
  };

  return (
    <Container>
      <h1>HRnet</h1>
      <Link to={'/data'}>View current employee</Link>
      <h1>Create Employee</h1>
      <Form
        onSubmit={(e) => {
          addEmployee();
          e.preventDefault();
          openModal();
        }}
      >
        <Column>
          <Label>First Name</Label>
          <Input
            onChange={(f) => setfirstName(f.target.value)}
            type="text"
            name="firstName"
          />
          <Label>Last Name</Label>
          <Input
            onChange={(f) => setLastName(f.target.value)}
            type="text"
            name="lastName"
          />
          <Label>Date of Birth</Label>
          <ReactDatePicker
            showMonthDropdown
            showYearDropdown
            adjustDateOnChange
            selected={birthDate}
            value={birthDate}
            name="birthDate"
            dateFormat="dd/MM/yyyy"
            onChange={(r) => setBirthDate(r)}
          ></ReactDatePicker>
          <DatePickerWrapperStyles />
          <Label>Start Date</Label>
          <ReactDatePicker
            showMonthDropdown
            adjustDateOnChange
            selected={startDate}
            name="startDate"
            dateFormat="dd/MM/yyyy"
            onSelect={(r) => setStartDate(r)}
          ></ReactDatePicker>
          <DatePickerWrapperStyles />
          <AdressContainer>
            <Legend>Adress</Legend>
            <Column>
              <Label>Street</Label>
              <Input
                onChange={(f) => setStreet(f.target.value)}
                type="text"
                name="street"
              />
              <Label>City</Label>
              <Input
                onChange={(f) => setCity(f.target.value)}
                type="text"
                name="City"
              />
              <Label>State</Label>
              <Select
                selected={state}
                onChange={(f) => setState(f.target.value)}
              >
                {states.map((f, index) => {
                  return (
                    <Option key={index} value={f.name}>
                      {f.name}
                    </Option>
                  );
                })}
              </Select>
              <Label>Zip Code</Label>
              <Input
                onChange={(f) => setZipCode(f.target.value)}
                type="number"
                name="Zip"
              />
            </Column>
          </AdressContainer>
          <Label>Department</Label>
          <Select
            selected={department}
            onChange={(d) => setDepartment(d.target.value)}
            onSelect={(f) => settttIt(f.target.value)}
            name="department"
            id="Department"
          >
            {departments.map((f, i) => {
              return (
                <Option key={i} value={f.name}>
                  {f}
                </Option>
              );
            })}
          </Select>
          <InputSubmit type={'submit'} />
        </Column>
      </Form>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Example Modal"
        style={modalStyles}
        ariaHideApp={false}
      >
        L'utilisateur a été créé avec succès
        <button
          onClick={closeModal}
          style={{
            backgroundColor: 'transparent',
            fontWeight: 'bold',
            color: 'darkgray',
          }}
        >
          {' '}
          X{' '}
        </button>
      </Modal>
    </Container>
  );
}
